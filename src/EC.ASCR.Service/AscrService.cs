﻿using System.Linq;
using AutoMapper;
using EC.ASCR.Data.Repository;
using EC.ASCR.Service.Contracts;
using EC.ASCR.Service.Contracts.Dto;
using EC.ASCR.Service.Contracts.Dto.Properties;
using EC.ASCR.Service.Contracts.Requests;
using EC.ASCR.Service.Contracts.Responses;

namespace EC.ASCR.Service
{
    public class AscrService : IAscrService
    {
        private readonly IRepository _repository;

        public AscrService(IRepository repository)
        {
            _repository = repository;
        }

        public StoreProfileResponse StoreProfile(StoreProfileRequest request)
        {
            if (request == null || string.IsNullOrWhiteSpace(request.FederatedId))
            {
                return new StoreProfileResponse();
            }

            var entriesToPush = request.Profile.Select(_ => new Data.Models.ProfileEntry
            {
                Key = NormalizeKey(_.Key),
                Context = Mapper.Map<Data.Models.Context>(request.Context),
                Label = request.Label,
                Property = Mapper.Map<Data.Models.Properties.Property>(_.Value)
            });

            _repository.Upsert(request.FederatedId, entriesToPush.ToList());

            return new StoreProfileResponse();
        }

        private static string NormalizeKey(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return string.Empty;
            }

            return key.ToLowerInvariant().Trim().Replace(" ", string.Empty).Replace("-", string.Empty).Replace("_", string.Empty);
        }

        public RetrieveMostRecentProfileResponse RetrieveMostRecentProfile(RetrieveMostRecentProfileRequest request)
        {
            if (request == null || string.IsNullOrWhiteSpace(request.FederatedId))
            {
                return new RetrieveMostRecentProfileResponse();
            }

            var user = _repository.RetrieveUser(request.FederatedId);

            if (user == null)
            {
                return new RetrieveMostRecentProfileResponse();
            }

            var entries = user.Profile.Select(_ => new ProfileEntryKeyValue
            {
                Key = _.Key,
                Context = Mapper.Map<Context>(_.Context),
                CreatedOn = _.CreatedOn,
                Label = _.Label,
                Property = Mapper.Map<Property>(_.Property),
            })
            .GroupBy(_ => _.Key)
            .Select(_ => _.OrderByDescending(x => x.CreatedOn).FirstOrDefault());

            return new RetrieveMostRecentProfileResponse
            {
                Profile = entries.ToList()
            };
        }

        public RetrieveProfileValuesResponse RetrieveProfileValues(RetrieveProfileValuesRequest request)
        {
            if (request == null || string.IsNullOrWhiteSpace(request.FederatedId))
            {
                return new RetrieveProfileValuesResponse();
            }

            var user = _repository.RetrieveUser(request.FederatedId);

            if (user == null)
            {
                return new RetrieveProfileValuesResponse();
            }

            var properties = user.Profile
                .Where(_ => _.Key == NormalizeKey(request.Key))
                .Select(_ => new ProfileEntryValue
                {
                    Context = Mapper.Map<Context>(_.Context),
                    CreatedOn = _.CreatedOn,
                    Label = _.Label,
                    Property = Mapper.Map<Property>(_.Property)
                });

            return new RetrieveProfileValuesResponse
            {
                ProfileValues = properties.ToList()
            };
        }
    }
}
