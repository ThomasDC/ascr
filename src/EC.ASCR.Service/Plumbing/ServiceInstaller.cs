﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EC.ASCR.Data.Plumbing;
using EC.ASCR.Service.Contracts;

namespace EC.ASCR.Service.Plumbing
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            ServiceDataMapper.Configure();
            container.Install(new RepositoryInstaller());
            container.Register(Component.For<IAscrService>().ImplementedBy<AscrService>());
        }
    }
}
