﻿using AutoMapper;
using EC.ASCR.Data.Models;
using EC.ASCR.Data.Models.Properties;

namespace EC.ASCR.Service.Plumbing
{
    public class ServiceDataMapper
    {
        public static void Configure()
        {
            // service -> data
            Mapper.CreateMap<Contracts.Dto.Context, Context>();
            Mapper.CreateMap<Contracts.Dto.Properties.Property, Property>()
                .Include<Contracts.Dto.Properties.BoolProperty, BoolProperty>()
                .Include<Contracts.Dto.Properties.ListStringProperty, ListStringProperty>()
                .Include<Contracts.Dto.Properties.StringProperty, StringProperty>()
                .Include<Contracts.Dto.Properties.IntProperty, IntProperty>();
            Mapper.CreateMap<Contracts.Dto.Properties.BoolProperty, BoolProperty>();
            Mapper.CreateMap<Contracts.Dto.Properties.ListStringProperty, ListStringProperty>();
            Mapper.CreateMap<Contracts.Dto.Properties.StringProperty, StringProperty>();
            Mapper.CreateMap<Contracts.Dto.Properties.IntProperty, IntProperty>();

            // data -> service
            Mapper.CreateMap<Context, Contracts.Dto.Context>();
            Mapper.CreateMap<Property, Contracts.Dto.Properties.Property>()
                .Include<BoolProperty, Contracts.Dto.Properties.BoolProperty>()
                .Include<ListStringProperty, Contracts.Dto.Properties.ListStringProperty>()
                .Include<StringProperty, Contracts.Dto.Properties.StringProperty>()
                .Include<IntProperty, Contracts.Dto.Properties.IntProperty>();
            Mapper.CreateMap<BoolProperty, Contracts.Dto.Properties.BoolProperty>();
            Mapper.CreateMap<ListStringProperty, Contracts.Dto.Properties.ListStringProperty>();
            Mapper.CreateMap<StringProperty, Contracts.Dto.Properties.StringProperty>();
            Mapper.CreateMap<IntProperty, Contracts.Dto.Properties.IntProperty>();
        }
    }
}
