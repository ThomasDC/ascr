﻿using System.Collections.Generic;
using EC.ASCR.Data.Models;

namespace EC.ASCR.Data.Repository
{
    public interface IRepository
    {
        void Upsert(string federatedId, IList<ProfileEntry> profileEntries);
        User RetrieveUser(string federatedId);
    }
}
