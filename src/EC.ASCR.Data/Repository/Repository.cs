﻿using System;
using System.Collections.Generic;
using EC.ASCR.Data.Models;
using MongoDB.Driver;

namespace EC.ASCR.Data.Repository
{
    public class Repository : IRepository
    {
        private readonly IMongoCollection<User> _users;

        public Repository(IMongoCollection<User> users)
        {
            _users = users;
        }

        public void Upsert(string federatedId, IList<ProfileEntry> profileEntries)
        {
            var now = DateTime.UtcNow;

            foreach (var entry in profileEntries)
            {
                entry.CreatedOn = now;
            }

            var update = Builders<User>.Update
                .PushEach(_ => _.Profile, profileEntries)
                .Set(_ => _.LastUpdatedOn, now);
            var filter = Builders<User>.Filter.Eq(_ => _.FederatedId, federatedId);
            var upsert = new UpdateOptions { IsUpsert = true };

            var response = _users.UpdateOneAsync(filter, update, upsert).Result;
        }

        public User RetrieveUser(string federatedId)
        {
            return _users.Find(_ => _.FederatedId == federatedId).FirstOrDefaultAsync().Result;
        }
    }
}
