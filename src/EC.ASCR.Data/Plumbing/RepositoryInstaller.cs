﻿using System.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EC.ASCR.Data.Models;
using EC.ASCR.Data.Repository;
using MongoDB.Driver;

namespace EC.ASCR.Data.Plumbing
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["mongodb"].ConnectionString;
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("ta_ascr");
            var users = database.GetCollection<User>("Users");

            container.Register(
                Component.For<MongoClient>().Instance(client),
                Component.For<IMongoCollection<User>>().Instance(users),
                Component.For<IRepository>().ImplementedBy<Repository.Repository>());
        }
    }
}
