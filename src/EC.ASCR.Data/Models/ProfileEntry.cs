﻿using System;
using EC.ASCR.Data.Models.Properties;

namespace EC.ASCR.Data.Models
{
    public class ProfileEntry
    {
        public string Key { get; set; }
        public Context Context { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Label { get; set; }
        public Property Property { get; set; }
    }
}