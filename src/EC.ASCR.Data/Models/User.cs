﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace EC.ASCR.Data.Models
{
    public class User
    {
        [BsonId]
        public string FederatedId { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public IList<ProfileEntry> Profile = new List<ProfileEntry>();
    }
}
