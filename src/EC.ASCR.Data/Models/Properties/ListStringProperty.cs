﻿using System.Collections.Generic;

namespace EC.ASCR.Data.Models.Properties
{
    public class ListStringProperty : Property
    {
        public IList<string> Value { get; set; }
    }
}