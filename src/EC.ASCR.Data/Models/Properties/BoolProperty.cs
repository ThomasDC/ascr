﻿namespace EC.ASCR.Data.Models.Properties
{
    public class BoolProperty : Property
    {
        public bool Value { get; set; }
    }
}
