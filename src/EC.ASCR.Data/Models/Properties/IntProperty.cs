﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EC.ASCR.Data.Models.Properties
{
    public class IntProperty : Property
    {
        public int Value { get; set; }
    }
}
