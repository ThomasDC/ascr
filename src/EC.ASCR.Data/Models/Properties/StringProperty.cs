namespace EC.ASCR.Data.Models.Properties
{
    public class StringProperty : Property
    {
        public string Value { get; set; }
    }
}