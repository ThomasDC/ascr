﻿using MongoDB.Bson.Serialization.Attributes;

namespace EC.ASCR.Data.Models.Properties
{
    [BsonKnownTypes(typeof(StringProperty), typeof(ListStringProperty), typeof(BoolProperty), typeof(IntProperty))]
    public abstract class Property
    {
    }
}