﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Castle.Windsor;
using EC.ASCR.Service.Contracts;
using EC.ASCR.Service.Contracts.Dto.Properties;
using EC.ASCR.Service.Contracts.Requests;
using EC.ASCR.Service.Plumbing;
using Context = EC.ASCR.Service.Contracts.Dto.Context;

namespace EC.ASCR.Client
{
    class Program
    {
        static void Main()
        {
            var container = BootstrapContainer();
            var service = container.Resolve<IAscrService>();

            var federatedId = Guid.NewGuid().ToString();

            Console.WriteLine("Federated id user: {0}\n\n", federatedId);

            Console.WriteLine("Simulating user added profile data by using the Multiplay service selector through P6");
            service.StoreProfile(FetchMultiplayP6Input(federatedId));
            Console.WriteLine("Profile keys stored in ASCR: {0}\n\n", string.Join(", ", NumberOfDistinctProfileKeysStored(service, federatedId)));

            Console.WriteLine("Simulating user added profile data by using the Energy service selector through SmartphonePortal");
            service.StoreProfile(FetchEnergySmartphoneportalInput(federatedId));
            Console.WriteLine("Profile keys stored in ASCR: {0}\n\n", string.Join(", ", NumberOfDistinctProfileKeysStored(service, federatedId)));

            Console.WriteLine("Simulating user added profile data by using the Energy switch website");
            service.StoreProfile(FetchEnergySwitchInput(federatedId));
            Console.WriteLine("Profile keys stored in ASCR: {0}\n\n", string.Join(", ", NumberOfDistinctProfileKeysStored(service, federatedId)));

            Console.WriteLine("Simulating user added profile data by using the Complain website");
            service.StoreProfile(FetchComplainInput(federatedId));
            Console.WriteLine("Profile keys stored in ASCR: {0}\n\n", string.Join(", ", NumberOfDistinctProfileKeysStored(service, federatedId)));

            var profile = service.RetrieveMostRecentProfile(new RetrieveMostRecentProfileRequest {FederatedId = federatedId}).Profile;
            const string dashes = "--------------------------------------------------------------------------------";
            Console.WriteLine("\n{0}\nPROFILE SUMMARY:", dashes);
            Console.WriteLine(dashes);
            foreach (var profileKeyValue in profile)
            {
                var visitor = new Visitor();
                profileKeyValue.Property.Accept(visitor);
                Console.WriteLine("{0}: {1} ({2})", profileKeyValue.Key, visitor, profileKeyValue.Context.Application);

                foreach (var value in service.RetrieveProfileValues(new RetrieveProfileValuesRequest {FederatedId = federatedId, Key = profileKeyValue.Key}).ProfileValues)
                {
                    visitor = new Visitor();
                    value.Property.Accept(visitor);
                    Console.WriteLine("\t- {0} ({1} - {2})", visitor, value.Context.Application, value.Context.Id);
                }

            }
            Console.WriteLine(dashes);

            Console.WriteLine("\n\nThat's all folks!");

            Console.ReadLine();
        }

        private static StoreProfileRequest FetchComplainInput(string federatedId)
        {
            return new StoreProfileRequest
            {
                Context = new Context
                {
                    Application = "Complain",
                    Id = "ComplainApp-android"
                },
                Profile = new Dictionary<string, Property>
                {
                    {"elecprovider", new StringProperty { Value = "electrabel"}}
                },
                FederatedId = federatedId
            };
        }

        private static StoreProfileRequest FetchEnergySwitchInput(string federatedId)
        {
            return new StoreProfileRequest
            {
                Context = new Context
                {
                    Application = "Switch",
                    Id = "switch-123456"
                },
                Profile = new Dictionary<string, Property>
                {
                    {"elecprovider", new StringProperty { Value = "Electrabel"}},
                    {"domicil", new BoolProperty { Value = false}}
                },
                FederatedId = federatedId
            };
        }

        private static StoreProfileRequest FetchEnergySmartphoneportalInput(string federatedId)
        {
            return new StoreProfileRequest
            {
                Context = new Context
                {
                    Application = "ServiceSelector",
                    Id = "srv://serviceselector/energy/belgium/20150315/smartphoneportal"
                },
                Label = "EnergyHome",
                Profile = new Dictionary<string, Property>
                {
                    {"Language", new StringProperty { Value = "nederlands"}},
                    {"Zip", new StringProperty { Value = "3000"}},
                    {"Domicil", new BoolProperty { Value = true}}
                },
                FederatedId = federatedId
            };
        }

        private static StoreProfileRequest FetchMultiplayP6Input(string federatedId)
        {
            return new StoreProfileRequest
            {
                Context = new Context
                {
                    Application = "ServiceSelector",
                    Id = "srv://serviceselector/multiplay/belgium/20150105/p6"
                },
                Label = "Home",
                Profile = new Dictionary<string, Property>
                {
                    {"internet_speed", new IntProperty { Value = 12}},
                    {"zip", new IntProperty { Value = 3000}},
                    {"Language", new StringProperty { Value = "NL"}}
                },
                FederatedId = federatedId
            };
        }

        private static IEnumerable<string> NumberOfDistinctProfileKeysStored(IAscrService service, string federatedId)
        {
            return service.RetrieveMostRecentProfile(new RetrieveMostRecentProfileRequest { FederatedId = federatedId }).Profile.Select(_ => _.Key).ToList();
        }

        private static IWindsorContainer BootstrapContainer()
        {
            var container = new WindsorContainer();
            container.Install(new ServiceInstaller());
            return container;
        }
    }
}
