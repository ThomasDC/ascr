﻿using System.Collections.Generic;
using EC.ASCR.Service.Contracts.Dto;
using EC.ASCR.Service.Contracts.Dto.Properties;

namespace EC.ASCR.Client
{
    public class Visitor : IVisitor
    {
        private readonly IList<string> _values = new List<string>();

        public void Visit(BoolProperty property)
        {
            _values.Add(string.Format("'{0}' (bool)", property.Value));
        }

        public void Visit(IntProperty property)
        {
            _values.Add(string.Format("'{0}' (int)", property.Value));
        }

        public void Visit(ListStringProperty property)
        {
            _values.Add(string.Format("[{0}]' (list of strings)", string.Join(";", property.Value)));
        }

        public void Visit(StringProperty property)
        {
            _values.Add(string.Format("'{0}' (string)", property.Value));
        }

        public override string ToString()
        {
            return string.Join(", ", _values);
        }
    }
}
