﻿namespace EC.ASCR.Service.Contracts.Dto.Properties
{
    public class IntProperty : Property
    {
        public int Value { get; set; }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
