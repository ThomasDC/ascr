namespace EC.ASCR.Service.Contracts.Dto.Properties
{
    public class StringProperty : Property
    {
        public string Value { get; set; }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}