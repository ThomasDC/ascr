﻿using System.Collections.Generic;

namespace EC.ASCR.Service.Contracts.Dto.Properties
{
    public class ListStringProperty : Property
    {
        public IList<string> Value { get; set; }

        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}