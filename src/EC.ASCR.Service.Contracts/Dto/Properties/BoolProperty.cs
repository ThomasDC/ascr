﻿namespace EC.ASCR.Service.Contracts.Dto.Properties
{
    public class BoolProperty : Property
    {
        public bool Value { get; set; }
        
        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
