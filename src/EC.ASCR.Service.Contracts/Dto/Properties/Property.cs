﻿
namespace EC.ASCR.Service.Contracts.Dto.Properties
{
    public abstract class Property
    {
        public abstract void Accept(IVisitor visitor);
    }
}