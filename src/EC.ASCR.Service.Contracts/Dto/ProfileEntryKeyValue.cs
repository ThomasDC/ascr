﻿using System;
using EC.ASCR.Service.Contracts.Dto.Properties;

namespace EC.ASCR.Service.Contracts.Dto
{
    public class ProfileEntryKeyValue
    {
        public string Key { get; set; }
        public Context Context { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Label { get; set; }
        public Property Property { get; set; }
    }
}
