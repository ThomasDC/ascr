﻿using EC.ASCR.Service.Contracts.Dto.Properties;

namespace EC.ASCR.Service.Contracts.Dto
{
    /// <summary>
    /// Using the Visitor pattern as an alternative to a bunch of switch case statements
    /// when iterating over a list of <see cref="Property"/>s gives us a compile time safety 
    /// net when new <see cref="Property"/> subclasses are added in the future.
    /// </summary>
    public interface IVisitor
    {
        void Visit(BoolProperty property);
        void Visit(IntProperty property);
        void Visit(ListStringProperty property);
        void Visit(StringProperty property);
    }
}
