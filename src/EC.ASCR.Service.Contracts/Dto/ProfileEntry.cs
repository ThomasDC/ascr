﻿using System.Collections.Generic;

namespace EC.ASCR.Service.Contracts.Dto
{
    public class ProfileEntry
    {
        public string Key { get; set; }
        public IList<ProfileEntryValue> Values = new List<ProfileEntryValue>();
    }
}