﻿namespace EC.ASCR.Service.Contracts.Dto
{
    public class Context
    {
        public string Application { get; set; }
        public string Id { get; set; }
    }
}