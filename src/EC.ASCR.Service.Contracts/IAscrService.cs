﻿using EC.ASCR.Service.Contracts.Requests;
using EC.ASCR.Service.Contracts.Responses;

namespace EC.ASCR.Service.Contracts
{
    public interface IAscrService
    {
        StoreProfileResponse StoreProfile(StoreProfileRequest request);

        RetrieveMostRecentProfileResponse RetrieveMostRecentProfile(RetrieveMostRecentProfileRequest request);
        RetrieveProfileValuesResponse RetrieveProfileValues(RetrieveProfileValuesRequest request);
    }
}
