﻿namespace EC.ASCR.Service.Contracts.Requests
{
    public abstract class Request
    {
        public string FederatedId { get; set; }
    }
}
