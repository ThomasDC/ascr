﻿namespace EC.ASCR.Service.Contracts.Requests
{
    public class RetrieveProfileValuesRequest : Request
    {
        public string Key { get; set; }
    }
}
