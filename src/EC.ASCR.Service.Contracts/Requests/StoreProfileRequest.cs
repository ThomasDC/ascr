using System.Collections.Generic;
using EC.ASCR.Service.Contracts.Dto;
using EC.ASCR.Service.Contracts.Dto.Properties;

namespace EC.ASCR.Service.Contracts.Requests
{
    public class StoreProfileRequest : Request
    {
        public Context Context { get; set; }
        public string Label { get; set; }
        public IDictionary<string, Property> Profile = new Dictionary<string, Property>();
    }
}