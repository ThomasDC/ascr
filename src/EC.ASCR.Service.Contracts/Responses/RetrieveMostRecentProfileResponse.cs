﻿using System.Collections.Generic;
using EC.ASCR.Service.Contracts.Dto;

namespace EC.ASCR.Service.Contracts.Responses
{
    public class RetrieveMostRecentProfileResponse
    {
        public IList<ProfileEntryKeyValue> Profile = new List<ProfileEntryKeyValue>();
    }
}
