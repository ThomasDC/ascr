﻿using System.Collections.Generic;
using EC.ASCR.Service.Contracts.Dto;

namespace EC.ASCR.Service.Contracts.Responses
{
    public class RetrieveProfileValuesResponse
    {
        public IList<ProfileEntryValue> ProfileValues { get; set; }
    }
}
